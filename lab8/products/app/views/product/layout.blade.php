<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
</head>

<body>
    List of products:
    @yield('content')
</body>
</html>