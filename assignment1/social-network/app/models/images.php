<?php
/*
The images variable is inside the function because it broke bring outside the
function
*/
function getRandomImage() {
    $images = array(
        "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png",
        "https://pixabay.com/static/uploads/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
        "https://awicsocialservices.ca/Content/images/board_of_directors/no_photo.jpg",
        "http://www.eduwedo.com/images/icons/bos_image.png",
        "https://pixabay.com/static/uploads/photo/2012/04/26/19/43/profile-42914_960_720.png",
        "https://medium.com/img/default-avatar.png",
        "https://image.freepik.com/free-icon/user-profile-icon_318-33925.png",
        "http://image005.flaticon.com/116/png/128/109/109518.png",
        "https://cdn3.iconfinder.com/data/icons/glyph/227/Business-512.png",
        "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png"
    );
    $randindex = rand(0, count($images) - 1);
    return $images[$randindex];
}

function getAllImages() {
    $images = array(
        "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png",
        "https://pixabay.com/static/uploads/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
        "https://awicsocialservices.ca/Content/images/board_of_directors/no_photo.jpg",
        "http://www.eduwedo.com/images/icons/bos_image.png",
        "https://pixabay.com/static/uploads/photo/2012/04/26/19/43/profile-42914_960_720.png",
        "https://medium.com/img/default-avatar.png",
        "https://image.freepik.com/free-icon/user-profile-icon_318-33925.png",
        "http://image005.flaticon.com/116/png/128/109/109518.png",
        "https://cdn3.iconfinder.com/data/icons/glyph/227/Business-512.png",
        "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png"
    );
    return $images;
}
?>