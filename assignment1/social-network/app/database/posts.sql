drop table if exists posts;

create table posts (
    id integer not null primary key autoincrement,
    postdate varchar(80) not null,
    name varchar(80) not null,
    message text default '',
    image text default '',
    label varchar(80) not null default 'Label not set'
);

insert into posts values (null, '19/04/2016', 'Jon Davenport', 'This is the first post added to the table ever, with no user!', 'https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png');
