drop table if exists comments;

create table comments (
    id integer not null primary key autoincrement,
    name varchar(80) not null,
    comment text default '',
    post_id integer not null
);

insert into comments values (null, 'Jon Davenport', 'This is the first comment on the first most!', 1);