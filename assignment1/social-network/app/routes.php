<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require "models/images.php";

Route::get('/', function()
{
	return Redirect::to('home');
});

/*
Gets all the posts from the database table Posts, and loads them into the home
page
*/
Route::get('home', function()
{
    $posts = getPosts();
    
    
    
    return View::make('home')->withPosts($posts);
});

/*
Called when a post is created and posted. It gets the post data from the inputs 
with the names specified 'name', 'message', and 'label' and passes these into
the add_post function.
*/
Route::post('add_post_action', function()
{
    $name = Input::get('name');
    $message = Input::get('message');
    $label = Input::get('label');
    
    if($name == '' || $message == '' || $label == '') {
        return Redirect::to('home');
    }
    
    $id = add_post($name, $message, $label);
    
    // checks if the id is valid and redirects to the home page to refresh
    if($id) {
        return Redirect::to('home');
    } else {
        die("Error adding post");
    }
});

/*
Called when a post is deleted, and takes the post id from the end of the route
and passes it through to the delete function
*/
Route::get('delete_post/{id}', function($id)
{
    delete_post($id);
    return Redirect::to('home');
});

/*
Will load the edit post page with the post data that was clicked on, getting the
id from the route. This route is generated in the link/button when loading all 
posts from
*/
Route::get('editpost/{id}', function($id)
{
    $post = getPost($id);
    return View::make('editpost')->withPost($post);
});

/*
This route is generated when a post is updated, it works similarly to adding a
post to the social network
*/
Route::post('editpost/update_post_action/{id}', function($id)
{
    // $name = Input::get('name');
    $message = Input::get('message');
    $label = Input::get('label');
    update_post($id, $message, $label);
    return Redirect::to('comments/' . $id);
});

/*
This works similar to the edit post route, by getting the id from the route and
using this to get all comments that link to this id, also displays the post that
was clicked on.
*/
Route::get('comments/{id}', function($id)
{
    $post = getPost($id);
    $comments = getComments($id);
    return View::make('comments')->withPost($post)->withComments($comments);
});


/*
This route is used to add a comment onto a post by using the post id from the 
route. Will reroute to the comments page to refresh and reset the route.
*/
Route::post('comments/add_comment_action/{id}', function($id)
{
    $name = Input::get('name');
    $comment = Input::get('comment');
    
    if($name == '' || $comment == '') {
        return Redirect::to('comments/' . $id);
    }
    
    $commentid = add_comment($name, $comment, $id);
    
    if ($commentid) {
        return Redirect::to('comments/' . $id);
    } else {
        die("Error commenting on the post");
    }
});

/*
Works similarly to deleting a post by getting the comment id and using the 
delete function, will redirect back to the comments page
*/
Route::get('delete_comment/{id}', function($id)
{
    delete_comment($id);
    return Redirect::back();
});

/*
Used to route to the photos display page.
*/
Route::get('photos', function()
{
    $images = getAllImages();
    return View::make('photos')->withPhotos($images);
});

Route::get('documentation', function()
{
    return View::make('doc');
});

/*
Get all posts from the posts table in the database
*/
function getPosts() {
    $sql = "select * from posts order by id desc";
    $posts = DB::select($sql);
    return $posts;
}

/*
Gets a single post with the passed in id
*/
function getPost($postid) {
    $sql = "select * from posts where id = ?";
    $posts = DB::select($sql, array($postid));
    $post = $posts[0];
    return $post;
}

/*
used to add a post to the table
*/
function add_post($name, $message, $label) {
    $date = date("d/m/Y");
    $image = getRandomImage();
    
    $sql = "insert into posts (postdate, name, message, image, label) values (?, ?, ?, ?, ?)";
    DB::insert($sql, array($date, $name, $message, $image, $label));
    
    $id = DB::getPdo()->lastInsertId();
    return $id;
}

/*
This function is used to update a post within the table with a given id, it will
update the name, message, and label
*/
function update_post($id, $message, $label) {
    $sql = "update posts set message = ?, label = ? where id = ?";
    DB::update($sql, array($message, $label, $id));
}

/*
Used to delete a single post by the given id
*/
function delete_post($postid) {
    $sql = "delete from posts where id = ?";
    $sqlcomments = "delete from comments where post_id = ?";
    DB::delete($sqlcomments, array($postid));
    DB::delete($sql, array($postid));
}

/*
gets all comments relating to the post id from the comments table
*/
function getComments($postid) {
    $sql = "select comments.id, comments.name, comment from comments where post_id = ?";
    $comments = DB::select($sql, array($postid));
    return $comments;
}

/*
Will add a comment to he comments table that relates to the post id
*/
function add_comment($name, $comment, $postid) {
    $sql = "insert into comments (name, comment, post_id) values (?, ?, ?)";
    DB::insert($sql, array($name, $comment, $postid));
    
    $id = DB::getPdo()->lastInsertId();
    return $id;
}

/*
will delete a comment with the comment id passed in, from the table
*/
function delete_comment($id) {
    $sql = "delete from comments where id = ?";
    DB::delete($sql, array($id));
}

function getCommentCount($postid) {
    $sql = "select * from comments where post_id = ?";
    $count = count(DB::select($sql, array($postid)));
    $commentword;
    if ($count == 1) {
        $commentword = " Comment";
    }else {
        $commentword = " Comments";
    }
    return $count . $commentword;
}