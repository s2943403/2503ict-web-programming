<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($search) {
    global $pms;
    
    if (!empty($search)) {
    	$results = array();
    	foreach ($pms as $pm) {
    		$name = stripos($pm['name'], $search) !== FALSE;
    		$year = strpos($pm['from'], $search) !== FALSE || strpos($pm['to'], $search) !== FALSE;
    		$state = stripos($pm['state'], $search) !== FALSE;
    		
    		if ($name || $year || $state) {
    			$results[] = $pm;
    		}
    	}
    	$pms = $results;
    }

    return $pms;
}
?>
