<?php
function getFriends() {
    $friends = array(
            array("name" => "Some Guy1", "dispimg" => "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png"),
            array("name" => "Some Guy2", "dispimg" => "https://pixabay.com/static/uploads/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"),
            array("name" => "Some Guy3", "dispimg" => "https://awicsocialservices.ca/Content/images/board_of_directors/no_photo.jpg"),
            array("name" => "Some Guy4", "dispimg" => "http://www.eduwedo.com/images/icons/bos_image.png"),
            array("name" => "Some Guy5", "dispimg" => "https://pixabay.com/static/uploads/photo/2012/04/26/19/43/profile-42914_960_720.png"),
            array("name" => "Some Guy6", "dispimg" => "https://medium.com/img/default-avatar.png"),
            array("name" => "Some Guy7", "dispimg" => "https://image.freepik.com/free-icon/user-profile-icon_318-33925.png"),
            array("name" => "Some Guy8", "dispimg" => "http://image005.flaticon.com/116/png/128/109/109518.png"),
            array("name" => "Some Guy9", "dispimg" => "https://cdn3.iconfinder.com/data/icons/glyph/227/Business-512.png"),
            array("name" => "Some Guy0", "dispimg" => "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png")
        );
    
    return $friends;
}
?>