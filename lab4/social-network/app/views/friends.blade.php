@extends('layout.master')

@section('title')
Social Network - Friends
@stop

@section('content')
<div class="container-fluid">
    <div class="col-md-4">
        <div class="row">
            <form class="well">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Search for Friends"/>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        @forelse ($friends as $friend)
        <div class="well">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" width="64" height="64" src="{{{ $friend['dispimg'] }}}" alt="display picture"></img>
                </div>
                <div class="media-body">
                    <p>
                        {{{ $friend['name'] }}}
                    </p>
                </div>
            </div>
        </div>
        @empty
            You have no friends, maybe you should go out more...
        @endforelse
    </div>
</div>
@stop