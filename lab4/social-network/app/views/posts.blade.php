@extends('layout.master')

@section('title')
Social Network - Posts
@stop

@section('content')
<div class="container-fluid">
    <div class="col-md-4">
        <div class="row">
            <form class="well">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name"/>
                </div>
                <div class="form-group">
                    <label for="name">Message:</label>
                    <textarea class="form-control" name="message" rows="4"></textarea>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        @forelse ($posts as $post)
        <div class="well">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" width="64" height="64" src="{{{ $post['image'] }}}" alt="display picture"></img>
                </div>
                <div class="media-body">
                    <p>
                        Message: {{{ $post['message'] }}}
                    </p>
                    <p>
                        Date: {{{ $post['date'] }}}
                    </p>
                </div>
            </div>
        </div>
        @empty
            No posts.
        @endforelse
    </div>
</div>
@stop