<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getLibraryUsers()
{
  $users = array(
      array('name' => 'Harold Holt', 'address' => '26 January 1966', 'phone' => '8740', 'email' => 'bolt@somethingcool'),
      array('name' => 'John McEwen', 'address' => '19 December 1967', 'phone' => '0864', 'email' => 'Mcewen@somethingcool'),
      array('name' => 'John Gorton', 'address' => '10 January 1968', 'phone' => '3490', 'email' => 'gorton@somethingcool'),
      array('name' => 'William McMahon', 'address' => '10 March 1971', 'phone' => '7032', 'email' => 'will@somethingcool'),
      array('name' => 'Gough Whitlam', 'address' => '5 December 1972', 'phone' => '9086', 'email' => 'whit@somethingcool'),
      array('name' => 'Malcolm Fraser', 'address' => '11 November 1975', 'phone' => '7654', 'email' => 'fraser@somethingcool'),
      array('name' => 'Bob Hawke', 'address' => '11 March 1983', 'phone' => '5678', 'email' => 'hawke@somethingnotcool'),
      array('name' => 'Paul Keating', 'address' => '20 December 1991', 'phone' => '4543', 'email' => 'keatingson?@something'),
      array('name' => 'John Howard', 'address' => '11 March 1996', 'phone' => '2123', 'email' => 'how@something'),
      array('name' => 'Kevin Rudd', 'address' => '3 December 2007', 'phone' => '3231', 'email' => 'rudd@something'),
      array('name' => 'Julia Gillard', 'address' => '24 June 2010', 'phone' => '2313', 'email' => 'gillard@something')
  );
  return $users;
}
?>

