<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/libraryUsers.php";


// Display search form
Route::get('/', function()
{
	return View::make('libraryusers.query');
});

// Perform search and display results
Route::get('search', function()
{
  $search = Input::get('searchquery');

  $results = search($search);

	return View::make('libraryusers.results')->withUsers($results)->withSearch($search);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($search) {
  $users = getLibraryUsers();
  
  if (!empty($search)) {
    $results = array();
    foreach ($users as $user) {
      $name = stripos($user['name'], $search) !== FALSE;
    	$phone = strpos($user['phone'], $search) !== FALSE;
    	$email = stripos($user['email'], $search) !== FALSE;
    		
    	if ($name || $phone || $email) {
    		$results[] = $user;
    	}
    }
    $users = $results;
  }
  
  return $users;
}