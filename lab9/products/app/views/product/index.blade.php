@extends('product.layout')
@section('title')
Products
@stop

@section('content')
    List of products:
    <ul>
        @foreach ($products as $product)
            <li>{{ link_to_route('product.show', $product->name, array($product->id)) }}</li>
        @endforeach
    </ul>
@stop