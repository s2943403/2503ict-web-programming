<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) {
			return View::make('user.index');
		} else {
			return View::make('user.login');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, User::$rules);
		if ($v->passes()) {
			$password = $input['password'];
			$encrypted = Hash::make($password);
			$user = new User;
			$user->username = $input['username'];
			$user->password = $encrypted;
			$user->save();
			$this->login();
			return Redirect::action('UserController@index');
		}else {
			// Session::put('create_error', 'Something is empty');
			return Redirect::action('UserController@create')->withErrors($v);
		}
	}
	
	public function login() {
		$inputs = Input::all();
		
		if (Auth::attempt(['username' => $inputs['username'], 'password' => $inputs['password']])) {
			return Redirect::action('UserController@index');
		}else {
			Session::put('login_error', 'Login failed');
			return Redirect::back();
		}
	}
	
	public function logout() {
		Auth::logout();
		return Redirect::action('UserController@index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function userExists($name, $password) {
		if ($name == "" || $password == "") {
			return true;
		}else {
			return false;
		}
		// $user = User::where('username', 'like', $name)->get();
		// if (isset($user)) {
		// 	return true;
		// }else {
		// 	return false;
		// }
	}

}
