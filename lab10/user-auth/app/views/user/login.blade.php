@extends('user.layout')
@section('content')
    {{ Form::open(array('url' => secure_url('user/login'))) }}
        {{ Form::label('username', 'Username:') }}
        {{ Form::text('username') }}
        {{ $errors->first('username') }}
        <p></p>
        {{ Form::label('password', 'Password:') }}
        {{ Form::password('password') }}
        {{ $errors->first('password') }}
        <p></p>
        {{ Form::submit('login') }}
    {{ Form::close() }}
    {{ Session::pull('login_error') }}
    <p>Don't have an account? {{ link_to_route('user.create', 'Create one!') }}</p>
@stop