@extends('user.layout')
@section('content')
    {{ Form::open(array('action' => 'UserController@store')) }}
        {{ Form::label('username', 'Username:') }}
        {{ Form::text('username') }}
        {{ $errors->first('username') }}
        <p></p>
        {{ Form::label('password', 'Password:') }}
        {{ Form::password('password') }}
        {{ $errors->first('password') }}
        <p></p>
        {{ Form::submit('Create') }}
    {{ Form::close() }}
    {{ Session::pull('create_error') }}
    <p>{{ link_to('user', 'Login') }}</p>
@stop