@extends('user.layout')
@section('content')
You are now logged in as: 
{{ Auth::user()->username }}
{{ link_to_route('user.logout', 'Logout') }}
@stop