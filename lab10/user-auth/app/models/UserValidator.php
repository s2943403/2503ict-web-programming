<?php
class UserValidator extends Eloquent {
    public static $rules = array(
        'username' => 'required|unique:users',
        'password' => 'required'
    );
}