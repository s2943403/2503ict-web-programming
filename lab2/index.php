<?php
$posts = array(
        array('date' => "27 Jan 2016", "message" => "HELLOW!", "image" => "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png"),
        array('date' => "28 Jan 2016", "message" => "NO!", "image" => "https://pixabay.com/static/uploads/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"),
        array('date' => "28 Jan 2016", "message" => "YES!", "image" => "https://awicsocialservices.ca/Content/images/board_of_directors/no_photo.jpg"),
        array('date' => "29 Jan 2016", "message" => "ok :(", "image" => "http://www.eduwedo.com/images/icons/bos_image.png"),
        array('date' => "30 Jan 2016", "message" => "asoijd", "image" => "https://pixabay.com/static/uploads/photo/2012/04/26/19/43/profile-42914_960_720.png"),
        array('date' => "31 Jan 2016", "message" => "lakjsdoja", "image" => "https://medium.com/img/default-avatar.png"),
        array('date' => "1 Feb 2016", "message" => "leij", "image" => "https://image.freepik.com/free-icon/user-profile-icon_318-33925.png"),
        array('date' => "19 Feb 2016", "message" => "lakjsd", "image" => "http://image005.flaticon.com/116/png/128/109/109518.png"),
        array('date' => "20 Jan 1", "message" => "alsjd", "image" => "https://cdn3.iconfinder.com/data/icons/glyph/227/Business-512.png"),
        array('date' => "20 Feb 2016", "message" => "How?", "image" => "https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png")
    );
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Social Network Name</a>
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Photos</a></li>
                        <li><a href="#">Friends</a></li>
                        <li><a href="#">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="col-md-4">
                <div class="row">
                    <form class="well">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Message:</label>
                            <textarea class="form-control" name="message" rows="4"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8">
                <?php
                        $randomNum = rand(1, 10);
                        for ($i = 0; $i < $randomNum; $i++) {
                ?>
                <div class="well">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" width="64" height="64" src=<?= $posts[$i] ["image"]?> alt="display picture"></img>
                        </div>
                        <div class="media-body">
                            <p>
                                Message: <?= $posts[$i] ["message"]?>
                            </p>
                            <p>
                                Date: <?= $posts[$i] ['date']?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </body>
</html>