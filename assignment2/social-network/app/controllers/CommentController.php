<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$v = Validator::make($inputs, Comment::$rules);
		
		if ($v->passes()) {
			$com = $inputs['comment'];
			$postid = $inputs['post_id'];
			$userid = Auth::id();
			
			$comment = new Comment;
			$comment->comment = $com;
			$commenter = User::find(Auth::user()->id);
			$post = Post::find($postid);
			$comment->post()->associate($post);
			$comment->user()->associate($commenter);
			$comment->save();
			
			return Redirect::to('comments/' . $postid);
		}else {
			return Redirect::to('comments/' . $postid);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	// /**
	//  * Remove the specified resource from storage.
	//  *
	//  * @param  int  $id
	//  * @return Response
	//  */
	public function destroy($id)
	{
		$comment = Comment::find($id);
		$postid = $comment->post->id;
		$comment->delete();
		print_r($postid);
		return Redirect::to('comments/' . $postid);
	}

}