<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$includes = $this->getIncludes();
		
		if (Auth::check()) {
			return View::make('users.userlist')->withIncludes($includes)->withUser(Auth::user());
		}
		
		return View::make('users.userlist')->withIncludes($includes);
	}
	
	// This is called when the Friends button is clicked, will get all friends
	// of the user and display them in userlist.blade.php
	public function listFriends() {
		$friends = $this->getFriends(Auth::id());
		$friendsArr = [];
		// print_r($friends[0]->user_id);
		// exit();
		foreach ($friends as $friend) {
			if ($friend->user_id != Auth::id()) {
				array_push($friendsArr, $friend->user_id);
			}elseif ($friend->friend_id != Auth::id()) {
				array_push($friendsArr, $friend->friend_id);
			}
		}
		
		$users = User::whereIn('id', $friendsArr)->get();
		return View::make('users.userlist')->withIncludes($this->getIncludes())->withUsers($users);
	}
	
	
	// Gets all friends of the user with the id $id
	private function getFriends($id) {
		$friends = Friend::where('friend_id', '=', $id);
		$friends->orWhere('user_id', '=', $id);
		return $friends->get();
	}
	
	
	// Called by the search field to find all users with the name from the input
	public function search() {
		$search = Input::get('name');
		
		$search = '%' . $search . '%';
		$users;
		if (Auth::check()) {
			$users = User::whereRaw('name LIKE ? AND id NOT LIKE ?', array($search, Auth::user()->id))->get();
		}else {
			$users = User::whereRaw('name LIKE ?', array($search))->get();
		}
		
		$includes = $this->getIncludes();
		
		if (Auth::check()) {
			return View::make('users.userlist')->withUsers($users)->withIncludes($includes)->withUser(Auth::user());
		}
		
		return View::make('users.userlist')->withUsers($users)->withIncludes($includes);
	}
	
	
	// decides what sidebar to display if the user is logged in or not
	private function getIncludes() {
		$includes = '';
		if (Auth::check()) {
			$includes = 'users.profile';
		}else {
			$includes = 'users.login';
		}
		return $includes;
	}
	
	
	// Signs a user in
	public function login() {
		$inputs = Input::all();
		
		if (Auth::attempt(['username' => $inputs['username'], 'password' => $inputs['password']])) {
			return Redirect::action('PostController@index');
		}else {
			Session::put('login_error', 'Login failed');
			return Redirect::action('PostController@index');
		}
	}
	
	
	// Logs out the user
	public function logout() {
		Auth::logout();
		return Redirect::to('home');
	}
	
	
	// Adds the user with the id $id as a friend
	public function addFriend($id) {
		$friends = new Friend;
		$friends->friend_id = $id;
		$friends->user_id = Auth::id();
		$friends->save();
		
		return Redirect::to('home');
	}
	
	
	
	// Removes the friend
	public function removeFriend($id) {
		$friend = Friend::where('user_id', '=', Auth::id())->where('friend_id', '=', $id);
		$friend->orWhere('user_id', '=', $id)->where('friend_id', '=', Auth::id());
		$friend->delete();
		
		return Redirect::to('home');
	}
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('profile.posts')->withIncludes('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$includes = $this->getIncludes();
		$inputs = Input::all();
		$v = Validator::make($inputs, User::$rules);
		
		if ($v->passes()) {
			$name = $inputs['name'];
			$password = Hash::make($inputs['password']);
			$username = $inputs['username'];
			$birthdate = $inputs['birthdate'];
			$datearr = explode('/', $birthdate);
			$day = $datearr[0];
			$datearr[0] = $datearr[2];
			$datearr[2] = $day;
			$birthdate = implode('-', $datearr);
			$user = new User;
			$user->name = $name;
			$user->password = $password;
			$user->username = $username;
			$user->birthdate = $birthdate;
			$user->image = $inputs['image'];
			$user->save();
			// return View::make('users.userlist')->withIncludes($includes);
			return Redirect::action('PostController@index');
		}else {
			return View::make('profile.posts')->withIncludes($includes)->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$posts = Post::where('user_id', '=', $user->id);
		$posts->where('state', '!=', 2);
		if (!isFriend($user->id)) {
			$posts->where('state', '!=', '1');
		}
		return View::make('profile.show')->withIncludes($this->getIncludes())->withProfileid($id)->withUser($user)->withPosts($posts->get());
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$posts = Post::where('user_id', '=', $user->id);
		$posts->where('state', '!=', 2);
		if (!isFriend($user->id)) {
			$posts->where('state', '!=', '1');
		}
		return View::make('profile.show')->withIncludes('users.edituser')->withProfileid($id)->withUser($user)->withPosts($posts->get());
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$includes = $this->getIncludes();
		$inputs = Input::all();
		$v = Validator::make($inputs, User::$updaterules);
		
		if ($v->passes()) {
			$name = $inputs['name'];
			$password = Hash::make($inputs['password']);
			$username = $inputs['username'];
			$birthdate = $inputs['birthdate'];
			$datearr = explode('/', $birthdate);
			$day = $datearr[0];
			$datearr[0] = $datearr[2];
			$datearr[2] = $day;
			$birthdate = implode('-', $datearr);
			$user = User::find($id);
			$user->name = $name;
			
			// checks if the password is empty for updating the user, 
			// if so doesn't override the current password
			if ($inputs['password'] != '') {
				$user->password = $password;
			}
			$user->username = $username;
			$user->birthdate = $birthdate;
			// Same as passwords but for images
			if ($inputs['image'] != '') {
				$user->image = $inputs['image'];
			}
			$user->save();
			
			return Redirect::action('UserController@show', [$id]);
		}else {
			$user = User::find($id);
			$posts = Post::where('user_id', '=', $user->id);
			$posts->where('state', '!=', 2);
			if (!isFriend($user->id)) {
				$posts->where('state', '!=', '1');
			}
			return View::make('profile.show')->withIncludes('users.edituser')->withProfileid($id)->withUser($user)->withPosts($posts->get())->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
