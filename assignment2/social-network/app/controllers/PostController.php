<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = $this->getPosts();
		return View::make('profile.posts')->withIncludes($this->getIncludes())->withPosts($posts);
	}
	
	
	
	// Decides which sidebar should be displayed
	private function getIncludes() {
		$includes = '';
		if (Auth::check()) {
			$includes = 'profile.postform';
		}else {
			$includes = 'users.login';
		}
		return $includes;
	}
	
	
	
	// get all posts with privacy permissions
	private function getPosts() {
		$posts = Post::where('state', '=', 0);
		if (Auth::check()) {
			$posts->orWhere(function($q) {
				$user = User::find(Auth::user()->id);
				
				$friendsArr = [];
				foreach($this->getFriends($user->id) as $friend) {
					array_push($friendsArr, $friend->user_id);
					array_push($friendsArr, $friend->friend_id);
				}
				$q->where('state', '=', 1)->whereIn('user_id', $friendsArr);
			});
			
			$posts->orWhere(function($q) {
				$q->where('state', '=', 1)->where('user_id', '=', Auth::id());
			});
			
			$posts->orWhere(function($q) {
				$q->where('state', '=', 2)->where('user_id', '=', Auth::id());
			});
		}
		$posts->orderBy('created_at', 'desc');
		$posts = $posts->get();
		return $posts;
	}
	
	
	// gets all friends of the user with the id $id
	private function getFriends($id) {
		$friends = Friend::where('friend_id', '=', $id);
		$friends->orWhere('user_id', '=', $id);
		return $friends->get();
	}
	


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$v = Validator::make($inputs, Post::$rules);
		
		if ($v->passes()) {
			$title = $inputs['name'];
			$message = $inputs['message'];
			$privacy = $inputs['privacy'];
			$post = new Post;
			$post->title = $title;
			$post->message = $message;
			$post->state = $privacy;
			$poster = User::find(Auth::user()->id);
			$post->user()->associate($poster);
			$post->save();
			
			return Redirect::to('home');
		}else {
			return View::make('profile.posts')->withIncludes($this->getIncludes())->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($postId)
	{
		// Used to show the post with all related comments
		$post = Post::find($postId);
	    $comments = $post->comments;
	    
	    
	    // Because paginate couldn't be used directly on $comments...
	    $commentArr = [];
	    foreach ($comments as $comment) {
	    	array_push($commentArr, $comment->post_id);
	    }
	    
	    $comments = Comment::whereIn('post_id', $commentArr)->paginate(6);
	    
	    
	    
	    $includes = 'users.login';
	    if(Auth::check()) {
	    	$includes = 'profile.commentsform';
	    }
	    return View::make('profile.comments')->withPost($post)->withComments($comments)->withIncludes($includes);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		return View::make('profile.posts')->withIncludes('profile.editpost')->withPosts($this->getPosts())->withPost($post);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Input::all();
		$v = Validator::make($inputs, Post::$rules);
		
		if ($v->passes()) {
			$title = $inputs['name'];
			$message = $inputs['message'];
			$privacy = $inputs['privacy'];
			$post = Post::find($id);
			$post->title = $title;
			$post->message = $message;
			$post->state = $privacy;
			$post->save();
			
			return Redirect::to('home');
		}else {
			return View::make('profile.posts')->withIncludes('profile.editpost')->withErrors($v)->withPost(Post::find($id))->withPosts($this->getPosts());
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// get all comments related to this id and delete these as well
		$post = Post::find($id);
		$comments = $post->comments;
		foreach($comments as $comment) {
			$comment->delete();
		}
		$post->delete();
		return Redirect::action('PostController@index');
	}


}
