<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('home');
});

// Route::get('user/{id}/posts', 'UserController@show');
Route::get('user/logout', array('as' => 'users.logout', 'uses' => 'UserController@logout'));
Route::get('user/{id}/add', 'UserController@addFriend');
Route::get('user/{id}/remove', 'UserController@removeFriend');
Route::get('friends', 'UserController@listFriends');


// Because delete likes to append 'postId' to the end of the route where 'postId' already exists
Route::post('comment/{id}', 'CommentController@destroy');
Route::post('user/search', 'UserController@search');
Route::post('user/login', 'UserController@login');
Route::post('home/{post}', 'PostController@destroy');
Route::post('edit/{post}', 'PostController@edit');
Route::post('edituser/{id}', 'UserController@edit');

Route::resource('user/{id}/profile', 'UserController@show');
Route::resource('user', 'UserController');
Route::resource('home', 'PostController');
Route::resource('edit', 'PostController');
Route::resource('edituser', 'UserController');
Route::resource('comments/{post}', 'PostController@show');
Route::resource('comment', 'CommentController');

function formatDate($date) {
    return $date->format("d/m/Y");
}

function formatBirth($date) {
    $now = new DateTime;
    $intervals = $date->diff($now);
    return $intervals->y;
}

function isFriend($id) {
    if (Auth::check()) {
    	$id2 = Auth::user()->id;
        $friends = Friend::whereRaw('(user_id like ? and friend_id like ?) or (user_id like ? and friend_id like ?)', array($id, $id2, $id2, $id))->get();
        if (count($friends) == 0) {
            return false;
        }
        return true;
    }else {
        return false;
    }
}

function getPrivacy($state) {
    switch ($state) {
        case 0:
            return 'Public';
        
        case 1:
            return 'Friends';
        
        case 2:
            return 'Private';
        
        default:
            return 'How?';
    }
}