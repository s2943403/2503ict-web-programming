<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function($table) {
			$table->increments('id');
			// the state will be 0 (private), 1 (friends) or 2 (public)
			$table->integer('state');
			$table->string('title');
			// used for linking to which user posted this post
			$table->integer('user_id');
			// $table->foreign('posted_by')->references('id')->on('users');
			// used for linking to which profile the post was posted on
			$table->text('message');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
