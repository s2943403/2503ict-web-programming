<?php
class UserSeeder extends seeder {
    public function run() {
		$birthdate = '07/05/1996';
		$datearr = explode('/', $birthdate);
		$day = $datearr[0];
		$datearr[0] = $datearr[2];
		$datearr[2] = $day;
		$birthdate = implode('-', $datearr);
        
		$user = new User;
		$user->name = 'Bob';
		$user->password = Hash::make('1234');
		$user->username = 'Bob@a.org';
		$user->birthdate = $birthdate;
		$user->save();
		
		$user = new User;
		$user->name = 'John';
		$user->password = Hash::make('1234');
		$user->username = 'John@a.org';
		$user->birthdate = $birthdate;
		$user->save();
		
		$user = new User;
		$user->name = 'Tom';
		$user->password = Hash::make('1234');
		$user->username = 'Tom@a.org';
		$user->birthdate = $birthdate;
		$user->save();
    }
}