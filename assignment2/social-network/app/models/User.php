<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	
	public static $rules = array(
        'name' => 'required',
        'username' => 'required|unique:users|email',
        'password' => 'required',
        'birthdate' => 'required|date_format:d/m/Y',
        'password2' => 'required|same:password',
        'image' => 'image'
    );
    
    public static $updaterules = array(
        'name' => 'required',
        'username' => 'required|email',
        'birthdate' => 'required|date_format:d/m/Y',
        'password2' => 'same:password',
        'image' => 'image'
    );
    
    public function getDates() {
		return array('created_at',
		'updated_at', 'birthdate');
	}

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('image', [
			'styles' => [
				'medium' => '300x300',
				'thumb' => '100x100'
			]
		]);
		parent::__construct($attributes);
	}
	
	public function posts() {
		return $this->hasMany('Post');
	}
	
	public function friends() {
		// return 'this is the friends function return';
		return $this->hasMany('Friend');
	}

}
