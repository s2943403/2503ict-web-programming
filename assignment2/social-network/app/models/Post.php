<?php
class Post extends Eloquent {
    // Rules go here
    public static $rules = array(
        'name' => 'required',
        'message' => 'required'
    );
    
    public function user() {
        return $this->belongsTo('User');
    }
    
    public function comments() {
        return $this->hasMany('Comment');
    }
}