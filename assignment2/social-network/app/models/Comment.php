<?php
class Comment extends Eloquent {
    // Rules go here
    public static $rules = array(
        'comment' => 'required'
    );
    
    public function post() {
        return $this->belongsTo('Post');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }
}