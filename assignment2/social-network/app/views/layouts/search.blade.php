@extends('layouts.master')

@section('search_content')
    
    {{ Form::open(array('method' => 'post', 'url' => 'user/search', 'class' => 'navbar-form')) }}
        <div class="form-group">
            {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Search Users')) }}
            {{ $errors->first('name') }}
        </div>
        {{ Form::submit('Search', array('class' => 'btn btn-default')) }}
    {{ Form::close() }}
    {{ Session::pull('create_error') }}
    
@stop