@section('sidebar')
    <div class="col-md-4">
        <div class="row">
            <div class="well">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" height="100" width="100" src="{{ asset(Auth::user()->image->url('thumb')) }}" alt="display picture"></img>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Logged in as: {{ Auth::user()->name }}</h4>
                    </div>
                    <div class="media-right">
                        {{ link_to("user/".Auth::user()->id."/profile", 'View Profile', array('class' => 'btn btn-default')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{ Form::open(array('action' => 'PostController@store', 'class' => 'well')) }}
                <div class='form-group'>
                    {{ Form::label('name', 'Title') }}
                    {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => $errors->first('name'))) }}
                </div>
                <div class='form-group'>
                    {{ Form::label('message', 'Message:') }}
                    {{ Form::textarea('message', '', array('class' => 'form-control', 'rows' => '4')) }}
                    {{ $errors->first('message') }}
                </div>
                <div class="form-group">
                    {{ Form::select('privacy', array(0 => 'Public', 1 => 'Friends', 2 => 'Private'), null, array('class' => 'form-control')) }}
                </div>
                <div class='form-group'>
                    {{ Form::reset('Reset', array('class' => 'btn btn-default')) }}
                    {{ Form::submit('Post', array('class' => 'btn btn-primary')) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop