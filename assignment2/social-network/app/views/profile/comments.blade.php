@extends('layouts.searchbutton')

@section('title')
Social Network - Comments
@stop

@section('content')
<div class="container-fluid">
    @include($includes)
    @yield('sidebar')
    <div class="col-md-8">
        <div class="well">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" width="100" height="100" src="{{{ asset($post->user->image->url('medium')) }}}" alt="display picture"></img>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{{ $post->user->name }}}</h4>
                    <p>
                        {{{ $post->message }}}
                    </p>
                    <p>
                        Posted on {{{ formatDate($post->created_at) }}}
                    </p>
                    <!--Loops through the comments variable to display all comments for the post-->
                    @forelse ($comments as $comment)
                        <hr>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $comment->user->name }}}</h4>
                            <p>
                                {{{ $comment->comment }}}
                            </p>
                        </div>
                        <!-- The owner of a post can delete all comments made on their post -->
                        @if (Auth::check())
                        @if (Auth::user()->id == $comment->user->id || Auth::user()->id == $comment->post->user->id)
                            <div class="media-right">
                                <p>
                                    <!--This delete button is a form so that it 
                                    runs the post method to delete a comment 
                                    rather than using Route::delete-->
                                    <form action="{{ url('comment/'.$comment->id) }}" method="POST">
                                        <button type="submit" id="delete-comment-{{ $comment->id }}" class="btn btn-danger">
                                            Delete
                                        </button>
                                    </form>
                                </p>
                            </div>
                        @endif
                        @endif
                    @empty
                        <p>
                            No Comments.
                        </p>
                    @endforelse
                    {{ $comments->links() }}
                </div>
                <div class="media-right">
                    <p>
                        <a href="#" class="btn btn-default">Edit</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@stop