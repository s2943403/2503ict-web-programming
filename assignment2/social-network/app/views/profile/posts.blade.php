@extends('layouts.searchbutton')

@section('title')
Social Network - Home
@stop

@section('content')
<div class="container-fluid">
    @include($includes)
    @yield('sidebar')
    <div class="col-md-8">
        <!-- Loops through the posts variable to display all the given posts -->
        @if (isset($posts))
            @forelse ($posts as $post)
                <div class="well">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" width="100" height="100" src="{{{ asset($post->user->image->url('medium')) }}}" alt="display picture"></img>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $post->title }}}</h4>
                            <p>
                                <b>{{{ $post->user->name }}}</b> posted on {{{ formatDate($post->created_at) }}}
                            </p>
                            <p>
                                {{{ $post->message }}}
                            </p>
                        </div>
                        <div class="media-right">
                            <p>
                                <!--<a href="#" class="btn btn-default">-->
                                <!--    Comments-->
                                <!--</a>-->
                                {{ link_to("comments/$post->id", 'Comments: '.count($post->comments), array('class' => 'btn btn-default')) }}
                            </p>
                            @if (Auth::check() && Auth::user()->id == $post->user->id)
                                <p>
                                    <form action="{{ url('edit/'.$post->id) }}" method="POST">
                                        <button type="submit" id="delete-post-{{ $post->id }}" class="btn btn-default">
                                            Edit
                                        </button>
                                    </form>
                                </p>
                                <p>
                                    <form action="{{ url('home/'.$post->id) }}" method="POST">
                                        <button type="submit" id="delete-post-{{ $post->id }}" class="btn btn-danger">
                                            Delete
                                        </button>
                                    </form>
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            @empty
                <div class="well">
                    There doesn't seem to be any posts. Be the first person to make one!
                </div>
            @endforelse
        @else
            <div class="well">
                There doesn't seem to be any posts. Be the first person to make one!
            </div>
        @endif
    </div>
</div>
@stop