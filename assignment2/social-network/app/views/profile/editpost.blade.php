@section('sidebar')
<div class="container-fluid">
    <div class="col-md-4">
        <div class="row">
            <div class="well">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" height="100" width="100" src="{{ asset(Auth::user()->image->url('thumb')) }}" alt="display picture"></img>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Logged in as: {{ Auth::user()->name }}</h4>
                    </div>
                    <div class="media-right">
                        {{ link_to("user/Auth::user()->id", 'View Profile', array('class' => 'btn btn-default')) }}
                    </div>
                </div>
            </div>
        </div>
        <!-- This row is to show the post being edited -->
        <div class="row">
            <div class="well">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" width="100" height="100" src="{{{ asset($post->user->image->url('medium')) }}}" alt="display picture"></img>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $post->title }}}</h4>
                            <p>
                                <b>{{{ $post->user->name }}}</b> posted on {{{ formatDate($post->created_at) }}}
                            </p>
                            <p>
                                {{{ $post->message }}}
                            </p>
                        </div>
                        <div class="media-right">
                            {{{ getPrivacy($post->state) }}}
                        </div>
                    </div>
                </div>
        </div>
        <!-- This is for the form to edit the post -->
        <div class="row">
            {{ Form::model($post, array('method' => 'PUT', 'route' => array('edit.update', $post->id), 'class' => 'well')) }}
            {{-- {{ Form::open(array('action' => 'PostController@update', 'class' => 'well')) }} --}}
                <div class='form-group'>
                    {{ Form::label('name', 'Title') }}
                    {{ Form::text('name', $post->title, array('class' => 'form-control')) }}
                    {{ $errors->first('name') }}
                </div>
                <div class='form-group'>
                    {{ Form::label('message', 'Message:') }}
                    {{ Form::textarea('message', $post->message, array('class' => 'form-control', 'rows' => '4')) }}
                    {{ $errors->first('message') }}
                </div>
                <div class="form-group">
                    {{ Form::select('privacy', array(0 => 'Public', 1 => 'Friends', 2 => 'Private'), $post->privacy, array('class' => 'form-control')) }}
                </div>
                <div class='form-group'>
                    {{ Form::reset('Reset', array('class' => 'btn btn-default')) }}
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop