@section('sidebar')
    <div class="col-md-4">
        <div class="row">
            <div class="well">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" height="100" width="100" src="{{ asset(Auth::user()->image->url('thumb')) }}" alt="display picture"></img>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Logged in as: {{ Auth::user()->name }}</h4>
                    </div>
                    <div class="media-right">
                        {{ link_to("user/".Auth::user()->id."/profile", 'View Profile', array('class' => 'btn btn-default')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop