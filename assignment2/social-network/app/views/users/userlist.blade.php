@extends('layouts.search')

@section('title')
Social Network - Comments
@stop

@section('content')
<div class="container-fluid">
    @include($includes)
    @yield('sidebar')
    <div class="col-md-8">
        <!-- Loops through the posts variable to display all the given posts -->
        @if (isset($users))
            @forelse ($users as $user)
                <div class="well">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" width="100" height="100" src="{{ asset($user->image->url('thumb')) }}" alt="Profile image not set"></img>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{ $user->name }}</h4>
                            <p>
                                {{{ formatBirth($user->birthdate) }}} years old <br>
                                Profile created on {{{ formatDate($user->created_at) }}}
                            </p>
                        </div>
                        <div class="media-right">
                            <p>
                            {{ link_to("user/$user->id", 'View Profile', array('class' => 'btn btn-default')) }}
                            </p>
                            <p>
                            @if (isFriend($user->id))
                            {{ link_to("user/$user->id/remove", 'Remove Friend', array('class' => 'btn btn-default')) }}
                            @else
                            {{ link_to("user/$user->id/add", 'Add Friend', array('class' => 'btn btn-default')) }}
                            @endif
                            </p>
                        </div>
                    </div>
                </div>
            @empty
                <div class="well">
                    User does not exist.
                </div>
            @endforelse
        @else
            <div class="well">
                Search for users.
            </div>
        @endif
    </div>
</div>
@stop