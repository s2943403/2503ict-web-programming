@section('sidebar')
    <div class="col-md-4">
        <div class="row">
            {{ Form::model($user, array('method' => 'PUT', 'route' => array('edituser.update', $user->id), 'class' => 'well form-horizontal', 'files' => true)) }}
                <div class='form-group'>
                    {{ Form::label('name', 'Full Name*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                        {{ Form::text('name', $user->name, array('class' => 'form-control')) }}
                        {{{ $errors->first("name") }}}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Form::label('username', 'Email*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                    {{ Form::text('username', $user->username, array('class' => 'form-control')) }}
                    {{{ $errors->first("username") }}}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Form::label('birthdate', 'Date of Birth*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                    {{ Form::text('birthdate', formatDate($user->birthdate), array('class' => 'form-control')) }}
                    {{{ $errors->first("birthdate") }}}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Form::label('password', 'Enter Password*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                    {{ Form::password('password', array('class' => 'form-control')) }}
                    {{{ $errors->first("password") }}}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Form::label('password2', 'Re-enter Password*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                    {{ Form::password('password2', array('class' => 'form-control', 'placeholder' => $errors->first("password2"))) }}
                    {{{ $errors->first("password2") }}}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Form::label('image', 'Profile Picture', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                    {{ Form::file('image', array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class='form-group'>
                    <div class="col-lg-8 col-lg-offset-4">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop