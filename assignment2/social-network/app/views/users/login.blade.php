@section('sidebar')
    <div class="col-md-4">
        <div class="row">
            {{ Form::open(array('url' => 'user/login', 'class' => 'well form-horizontal')) }}
                <div class='form-group'>
                        {{ Form::label('username', 'Email*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                        {{ Form::text('username', '', array('class' => 'form-control', 'placeholder' => $errors->first("username"))) }}
                    </div>
                </div>
                <div class='form-group'>
                        {{ Form::label('password', 'Enter Password*', array('class' => "col-lg-4 control-label")) }}
                    <div class="col-lg-8">
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => $errors->first("password"))) }}
                    </div>
                </div>
                <div class='form-group'>
                    <div class="col-lg-8 col-lg-offset-4">
                        {{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
                        {{ HTML::linkAction('UserController@create', 'Create an account') }}
                    </div>
                </div>
                <div class='form-group'>
                    {{ Session::pull('login_error') }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop